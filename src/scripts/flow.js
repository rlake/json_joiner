// constants capturing our different divs
const radios_paragraph = document.getElementById("radios_paragraph")
const radios_center = document.getElementById("radios_center");
const radios_left = document.getElementById("radios_left");
const radios_right = document.getElementById("radios_right");
const radios_submit = document.getElementById("radios_submit");

const view_key_select_paragraph = document.getElementById("view_key_paragraph");
const view_key_select_left = document.getElementById("view_key_selection_left")
const view_key_select_right = document.getElementById("view_key_selection_right")
const view_key_submit = document.getElementById("view_key_submit")

const join_type_paragraph = document.getElementById("join_type_paragraph")
const join_type_submit = document.getElementById("join_type_submit")

const download_div = document.getElementById("download_json")

// Capture JSON input
function capture_json() {

    // Clear any elements that were created with the last JSON comparison
    clear_elements([
        "radios_paragraph",
        "radios_center",
        "radios_left",
        "radios_right",
        "radios_submit",
        "view_key_paragraph",
        "view_key_selection_left",
        "view_key_selection_right",
        "view_key_submit",
        "join_type_paragraph",
        "join_type_submit",
        "download_json"
    ]);

    // Confirm the strings can be parsed into JSON
    json_a = document.getElementById("input_form_a").value;
    json_b = document.getElementById("input_form_b").value;
    json_a_check = json_confirm(json_a);
    json_b_check = json_confirm(json_b);
    if (json_a_check == false && json_b_check == false) {
        alert("Neither of those strings are valid JSON.");
    } else if (json_a_check == false && json_b_check == true) {
        alert("The input on the left isn't valid JSON.");
    } else if (json_a_check == true && json_b_check == false) {
        alert("The input on the right isn't valid JSON.");
    } else if (json_a_check == true && json_b_check == true) {
        join_key_selection(json_a, json_b)
        // json_key_compare(json_a, json_b);
    }
}

// Check for key matches in the two JSON files
// function json_key_compare(json_a, json_b) {
//     json_a = JSON.parse(json_a);
//     json_b = JSON.parse(json_b);
//     json_a_keys = Object.keys(json_a[0]);
//     json_b_keys = Object.keys(json_b[0]);
//     matching_keys = [];
//     for (var i = 0; i < json_a_keys.length; i++) {
//         key_check = json_b_keys.indexOf(json_a_keys[i]);
//         // indexOf returns the index when there's a match, and -1 when there isn't
//         if (key_check > -1) {
//             matching_keys.push(json_a_keys[i]);
//         }
//     }
//     if (matching_keys.length == 0) {
//         no_match_confirm = confirm(
//             "There are no matching keys in your JSON. \n\nDo you want to join on a non-matching key?"
//         );
//         if (no_match_confirm == false) {
//             return;
//         } else if (no_match_confirm == true) {
//             choice = "no-match";
//         }
//     } else if (matching_keys.length > 0) {
//         key_string = "";
//         for (var i = 0; i < matching_keys.length; i++) {
//             key_string += '"' + matching_keys[i] + '", ';
//         }
//         key_string = key_string.slice(0, -2);
//         matched_key_choice = confirm(
//             "The following keys match in your two JSON blobs.\n\n" +
//             key_string +
//             "\n\nDo you want to join on one of these keys?"
//         );
//         if (matched_key_choice == true) {
//             choice = "match";
//         } else if (matched_key_choice == false) {
//             choice = "no-match";
//         }
//     }
//     join_key_selection(
//         choice,
//         matching_keys,
//         json_a,
//         json_b,
//         json_a_keys,
//         json_b_keys
//     );
// }

// Select which key to join on
function join_key_selection(
    json_a,
    json_b
) {

    json_a = JSON.parse(json_a);
    json_b = JSON.parse(json_b);
    json_a_keys = Object.keys(json_a[0]);
    json_b_keys = Object.keys(json_b[0]);

    // if (choice == "match") {
    //     var paragraph = createNode("p")
    //     paragraph.innerHTML = "What key do you want to join on?"
    //     append(radios_paragraph, paragraph)
    //     for (i = 0; i < matching_keys.length; i++) {
    //         var radio = createNode("input");
    //         var label = createNode("label");
    //         radio.type = "radio";
    //         radio.name = "matching_key_radios";
    //         radio.id = matching_keys[i] + "_radio";
    //         radio.value = matching_keys[i];
    //         label.for = matching_keys[i] + "_radio";
    //         label.innerHTML = matching_keys[i];
    //         append(radios_center, label);
    //         append(label, radio);
    //     }
    //     // var div = createNode("div");
    //     // div.id = "matching_submit_div";
    //     // append(submit_div, div);
    //     // var matching_submit_div = document.getElementById("matching_submit_div");
    //     var button = createNode("button");
    //     button.innerHTML = "Submit";
    //     append(radios_submit, button);
    //     button.addEventListener("click", function () {
    //         match_radio_checked = false;
    //         join_keys = {
    //             json_source: {}
    //         };
    //         matching_radios = document.getElementsByName("matching_key_radios");
    //         for (i = 0; i < matching_radios.length; i++) {
    //             if (matching_radios[i].checked) {
    //                 match_radio_checked = true;
    //                 join_keys.json_source.json_a = {
    //                     key: matching_radios[i].value
    //                 };
    //                 join_keys.json_source.json_b = {
    //                     key: matching_radios[i].value
    //                 };
    //                 break;
    //             }
    //         }
    //         if (match_radio_checked == true) {
    //             select_view_keys(join_keys, choice, json_a, json_b);
    //         } else {
    //             alert("You need to select a matching key to join your data.");
    //         }
    //     });
    // } else if (choice == "no-match") {

        var paragraph = createNode("p")
        paragraph.innerHTML = "What keys do you want to join on?"
        append(radios_paragraph, paragraph)
        no_match_keys = [];
        for (i = 0; i < json_a_keys.length; i++) {
            // if (matching_keys.indexOf(json_a_keys[i]) == -1) {
                var radio = createNode("input");
                var label = createNode("label");
                radio.type = "radio";
                radio.name = "left_div_radios";
                radio.id = json_a_keys[i];
                radio.value = json_a_keys[i];
                label.for = json_a_keys[i];
                label.innerHTML = json_a_keys[i];
                append(radios_left, label);
                append(label, radio);
            // }
        }
        for (i = 0; i < json_b_keys.length; i++) {
            // if (matching_keys.indexOf(json_b_keys[i]) == -1) {
                var radio = createNode("input");
                var label = createNode("label");
                radio.type = "radio";
                radio.name = "right_div_radios";
                radio.id = json_b_keys[i];
                radio.value = json_b_keys[i];
                label.for = json_b_keys[i];
                label.innerHTML = json_b_keys[i];
                append(radios_right, label);
                append(label, radio);
            // }
        }
        var button = createNode("button");
        button.innerHTML = "Submit";
        append(radios_submit, button);
        button.addEventListener("click", function () {
            left_radios_checked = false;
            right_radios_checked = false;
            join_keys = {
                json_source: {}
            };

            left_radios = document.getElementsByName("left_div_radios");
            for (i = 0; i < left_radios.length; i++) {
                if (left_radios[i].checked) {
                    left_radios_checked = true;
                    join_keys.json_source.json_a = {
                        key: left_radios[i].value
                    };
                    break;
                }
            }
            right_radios = document.getElementsByName("right_div_radios");
            for (i = 0; i < right_radios.length; i++) {
                if (right_radios[i].checked) {
                    right_radios_checked = true;
                    join_keys.json_source.json_b = {
                        key: right_radios[i].value
                    };
                    break;
                }
            }
            if (left_radios_checked == true && right_radios_checked == true) {
                select_view_keys(join_keys, json_a, json_b);
            } else {
                alert("You need to select a key from each JSON blob.");
            }
        });
    }
// }

function select_view_keys(join_keys, json_a, json_b) {

    clear_elements([
        "view_key_paragraph",
        "view_key_selection_left",
        "view_key_selection_right",
        "view_key_submit",
        "join_type_paragraph",
        "join_type_submit",
        "download_json"
    ])

    var paragraph = createNode("p");
    paragraph.innerHTML = "What keys do you want to include in the joined array?";
    append(view_key_select_paragraph, paragraph);
    json_a_keys = Object.keys(json_a[0]);
    json_b_keys = Object.keys(json_b[0]);

    checkbox_ids = [];

    for (i = 0; i < json_a_keys.length; i++) {
        var checkbox = createNode("input");
        var label = createNode("label");
        checkbox.type = "checkbox";
        checkbox.name = "checkbox";
        checkbox.id = json_a_keys[i] + "_checkbox_a";
        checkbox.value = json_a_keys[i];
        label.for = json_a_keys[i] + "_checkbox_a";
        label.innerHTML = json_a_keys[i];
        append(view_key_select_left, label);
        append(label, checkbox);
        checkbox_ids.push({
            id: json_a_keys[i] + "_checkbox_a",
            key: json_a_keys[i],
            json_source: "json_a"
        });
    }
    for (i = 0; i < json_b_keys.length; i++) {
        var checkbox = createNode("input");
        var label = createNode("label");
        checkbox.type = "checkbox";
        checkbox.name = "checkbox";
        checkbox.id = json_b_keys[i] + "_checkbox_b";
        checkbox.value = json_b_keys[i];
        label.for = json_b_keys[i] + "_checkbox_b";
        label.innerHTML = json_b_keys[i];
        append(view_key_select_right, label);
        append(label, checkbox);
        checkbox_ids.push({
            id: json_b_keys[i] + "_checkbox_b",
            key: json_b_keys[i],
            json_source: "json_b"
        });
    }
    var button = createNode("button");
    button.innerHTML = "Submit";
    append(view_key_submit, button);
    button.addEventListener("click", function () {

        keys_to_include = {
            json_source: {}
        }
        keys_to_include.json_source.json_a = []
        keys_to_include.json_source.json_b = []

        for (i = 0; i < checkbox_ids.length; i++) {
            if (document.getElementById(checkbox_ids[i].id).checked == true) {
                // keys_to_include.json_source.json_a.push(checkbox_ids.key[i]);
                json_list = checkbox_ids[i].json_source
                keys_to_include.json_source[json_list].push(
                    checkbox_ids[i].key);
            }
        }
        select_join_type(join_keys, keys_to_include, json_a, json_b);
    });
}

function select_join_type(join_keys, keys_to_include, json_a, json_b) {

    clear_elements(["join_type_paragraph", "join_type_submit", "download_json"]);

    var paragraph = createNode("p");
    paragraph.innerHTML = "What type of join do you want to do?";
    var inner_join = createNode("button");
    inner_join.innerHTML = "Inner join";
    inner_join.value = "inner_join";
    inner_join.class = "join_buttons"
    var full_outer_join = createNode("button");
    full_outer_join.innerHTML = "Full outer join";
    full_outer_join.value = "full_outer_join";
    full_outer_join.class = "join_buttons"
    var left_outer_join = createNode("button");
    left_outer_join.innerHTML = "Left outer join";
    left_outer_join.value = "left_outer_join";
    left_outer_join.class = "join_buttons"
    var right_outer_join = createNode("button");
    right_outer_join.innerHTML = "Right outer join";
    right_outer_join.value = "right_outer_join";
    right_outer_join.class = "join_buttons"
    append(join_type_paragraph, paragraph);
    append(join_type_submit, inner_join);
    append(join_type_submit, left_outer_join);
    append(join_type_submit, right_outer_join);
    append(join_type_submit, full_outer_join);

    inner_join.addEventListener("click", function () {
        join_json(join_keys, "inner", keys_to_include, json_a, json_b);
    });
    left_outer_join.addEventListener("click", function () {
        join_json(join_keys, "left_outer", keys_to_include, json_a, json_b);
    });
    right_outer_join.addEventListener("click", function () {
        join_json(join_keys, "right_outer", keys_to_include, json_a, json_b);
    });
    full_outer_join.addEventListener("click", function () {
        join_json(join_keys, "full_outer", keys_to_include, json_a, json_b);
    });
}