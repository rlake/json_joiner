function clear_elements(div_array) {
    for (i = 0; i < div_array.length; i++) {
        div = document.getElementById(div_array[i]);
        if (div != null) {
            while (div.firstChild) {
                div.removeChild(div.firstChild);
            }
        }
    }
}

function createNode(element) {
    return document.createElement(element);
}

function append(parent, el) {
    return parent.appendChild(el);
}

function json_confirm(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}