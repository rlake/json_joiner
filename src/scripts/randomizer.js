function create_json_a() {

    json_a_array = []

    json_a_string_keys = {
        "status": ["active", "inactive"],
        "price_plan": ["basic", "standard", "advanced", "plus"],
        "country": ["canada", "usa", "japan", "uk", "france", "australia", "china", "brazil", "nigeria"]
    }

    // create 50 customer ids in an array
    customer_ids = []
    for (i = 1; i < 15 + 1; i++) {
        customer_ids.push(i)
    }

    // shuffle them
    customer_ids_shuffled = []
    while (customer_ids.length > 0) {
        rand_int = Math.floor(Math.random() * customer_ids.length)
        customer_ids_shuffled.push(customer_ids[rand_int])
        customer_ids.splice(rand_int, 1)
    }

    // add a number between 5 and 15 customers to our customer_ids list thatw e'll generate JSON objects for
    num_of_customers = Math.floor(Math.random() * (15 - 5) + 5)
    for (i = 0; i < num_of_customers; i++) {
        customer_ids.push(customer_ids_shuffled[i])
    }

    for (i = 0; i < num_of_customers; i++) {
        temp_object = {}

        temp_object["customer_id"] = customer_ids[i]

        rand_status = Math.floor(Math.random() * json_a_string_keys.status.length)
        temp_object["vendor_status"] = json_a_string_keys.status[rand_status]

        rand_pp = Math.floor(Math.random() * json_a_string_keys.price_plan.length)
        temp_object["price_plan"] = json_a_string_keys.price_plan[rand_pp]

        rand_country = Math.floor(Math.random() * json_a_string_keys.country.length)
        temp_object["country"] = json_a_string_keys.country[rand_country]

        json_a_array.push(temp_object)
    }

    return json_a_array
}

function create_json_b() {

    json_b_array = []

    json_b_string_keys = {
        "product_type": ["clothing", "fragrance", "electronics", "alcohol", "services", "crafts", "hobby", "miscellaneous"],
        "payment_method": ["visa", "amex", "mastercard", "apple_pay", "bitcoin", "google_wallet"]
    }

    // randomize the number of orders
    num_of_orders = Math.floor(Math.random() * (30 - 15) + 15)

    // create our list of 50 customers, but don't shuffle them because we don't care about duplicates
    customer_ids = []
    for (i = 1; i < 15 + 1; i++) {
        customer_ids.push(i)
    }

    // create our order ids so they're unique
    raw_order_ids = []
    for (i = 1; i < 50 + 1; i++) {
        raw_order_ids.push(i)
    }

    // shuffle the orders and we'll pull from them when we're creating the JSON objects
    order_ids = []
    while (raw_order_ids.length > 0) {
        rand_int = Math.floor(Math.random() * raw_order_ids.length)
        order_ids.push(raw_order_ids[rand_int])
        raw_order_ids.splice(rand_int, 1)
    }


    for (i = 0; i < num_of_orders; i++) {
        temp_object = {}

        rand_customer = Math.floor(Math.random() * customer_ids.length)
        temp_object["cid"] = customer_ids[rand_customer]

        rand_product = Math.floor(Math.random() * json_b_string_keys.product_type.length)
        temp_object["product_type"] = json_b_string_keys.product_type[rand_product]

        rand_mop = Math.floor(Math.random() * json_b_string_keys.payment_method.length)
        temp_object["method_of_payment"] = json_b_string_keys.payment_method[rand_mop]

        rand_order_amount = parseFloat((Math.random() * (150 - 5) + 5).toFixed(2))
        temp_object["order_amount"] = rand_order_amount

        temp_object["order_id"] = order_ids[i]

        taxes = parseFloat((rand_order_amount * parseFloat((Math.random() * (0.15 - 0.02) + 0.02).toFixed(2))).toFixed(2))
        temp_object["taxes"] = taxes

        json_b_array.push(temp_object)
    }

    return json_b_array
}

const left_text_area = document.getElementById("input_form_a");
const right_text_area = document.getElementById("input_form_b");

function generate_random_json() {
    left_text_area.value = ""
    right_text_area.value = ""

    json_a = create_json_a()
    json_b = create_json_b()

    left_text_area.value = JSON.stringify(json_a, null, 2)
    right_text_area.value = JSON.stringify(json_b, null, 2)
}