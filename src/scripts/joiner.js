function join_json(join_keys, join_type, keys_to_include, json_a, json_b) {

    clear_elements(["download_json"])

    json_a_join_key = join_keys.json_source.json_a.key;
    json_b_join_key = join_keys.json_source.json_b.key;

    // Confirm if the data being sorted is an integer or a string or another, unsupported format
    // Use the built in Javascript sort function which has much better than O(n^2) performance
    if (Number.isInteger(json_a[0][json_a_join_key]) && Number.isInteger(json_b[0][json_b_join_key])) {
        json_a.sort(function (a, b) {
            return a[json_a_join_key] - b[json_a_join_key];
        });
        json_b.sort(function (a, b) {
            return a[json_b_join_key] - b[json_b_join_key];
        });

    } else if (typeof (json_a[0][json_a_join_key]) == "string" && typeof (json_b[0][json_b_join_key]) == "string") {
        json_a.sort(function (a, b) {
            return a[json_a_join_key] > b[json_a_join_key]
        });
        json_b.sort(function (a, b) {
            return a[json_b_join_key] > b[json_b_join_key]
        });

    } else {
        alert(
            "Either the data type of the two keys you specified don't match, or you've specified a data type other than string or integer and that isn't supported."
        )
        return
    }



    left_position = 0;
    right_position = 0;

    new_joined_object = [];

    json_a_keys = Object.keys(json_a[0]);
    json_b_keys = Object.keys(json_b[0]);

    json_a_view_keys = keys_to_include.json_source.json_a
    json_b_view_keys = keys_to_include.json_source.json_b

    if (join_type == "inner") {

        while (true) {
            if (
                json_a[left_position][json_a_join_key] ==
                json_b[right_position][json_b_join_key]
            ) {

                temp_object = {};

                for (i = 0; i < json_a_view_keys.length; i++) {
                    temp_object[(json_a_view_keys[i])] = json_a[left_position][(json_a_view_keys[i])];
                }
                for (i = 0; i < json_b_view_keys.length; i++) {
                    temp_object[(json_b_view_keys[i])] = json_b[right_position][(json_b_view_keys[i])];
                }
                new_joined_object.push(temp_object);
                right_position++;

            } else if (
                json_a[left_position][json_a_join_key] <
                json_b[right_position][json_b_join_key]
            ) {

                left_position++;

            } else if (

                json_a[left_position][json_a_join_key] >
                json_b[right_position][json_b_join_key]
            ) {

                right_position++;
            }
            // Condition to break the infinite loop
            if (left_position >= json_a.length || right_position >= json_b.length) {
                break;
            }
        }
    }
    if (join_type == "full_outer") {

        left_item_added = false;
        right_item_added = false;

        while (true) {
            if (
                json_a[left_position][json_a_join_key] ==
                json_b[right_position][json_b_join_key]
            ) {
                temp_object = {};

                for (i = 0; i < json_a_view_keys.length; i++) {
                    temp_object[json_a_view_keys[i]] = json_a[left_position][json_a_view_keys[i]];
                }
                for (i = 0; i < json_b_view_keys.length; i++) {
                    temp_object[json_b_view_keys[i]] = json_b[right_position][json_b_view_keys[i]];
                }
                new_joined_object.push(temp_object);

                left_item_added = true;
                right_item_added = true;
                right_position++;
                right_item_added = false;

            } else if (
                json_a[left_position][json_a_join_key] <
                json_b[right_position][json_b_join_key]
            ) {
                if (left_item_added == false) {
                    temp_object = {};

                    for (i = 0; i < json_a_view_keys.length; i++) {
                        temp_object[json_a_view_keys[i]] = json_a[left_position][json_a_view_keys[i]];
                    }
                    for (i = 0; i < json_b_view_keys.length; i++) {
                        if (json_a_view_keys.indexOf(json_b_view_keys[i]) > -1) { continue }
                        temp_object[json_b_view_keys[i]] = null;
                    }
                    new_joined_object.push(temp_object);
                    left_item_added = true;
                }

                left_position++;
                left_item_added = false;

            } else if (
                json_a[left_position][json_a_join_key] >
                json_b[right_position][json_b_join_key]
            ) {
                if (right_item_added == false) {
                    temp_object = {};

                    for (i = 0; i < json_a_view_keys.length; i++) {
                        temp_object[json_a_view_keys[i]] = null;
                    }
                    for (i = 0; i < json_b_view_keys.length; i++) {
                        temp_object[json_b_view_keys[i]] =
                            json_b[right_position][json_b_view_keys[i]];
                    }
                }
                new_joined_object.push(temp_object);
                right_item_added = true;
                right_position++;
                right_item_added = false;
            }


            // Condition to break the infinite loop
            if (left_position >= json_a.length || right_position >= json_b.length) {

                while (true) {
                    if (left_position >= json_a.length && right_position < json_b.length) {
                        temp_object = {};

                        for (i = 0; i < json_a_view_keys.length; i++) {
                            temp_object[json_a_view_keys[i]] = null;
                        }
                        for (i = 0; i < json_b_view_keys.length; i++) {
                            temp_object[json_b_view_keys[i]] =
                                json_b[right_position][json_b_view_keys[i]];
                        }
                        new_joined_object.push(temp_object);
                        right_position++;
                        if (right_position >= json_b.length) {
                            break;
                        }
                    } else if (right_position >= json_b.length && left_position < json_a.length) {
                        temp_object = {};

                        for (i = 0; i < json_a_view_keys.length; i++) {
                            temp_object[json_a_view_keys[i]] = json_a[left_position][json_a_view_keys[i]];
                        }
                        for (i = 0; i < json_b_view_keys.length; i++) {
                            if (json_a_view_keys.indexOf(json_b_view_keys[i]) > -1) { continue }
                            temp_object[json_b_view_keys[i]] = null;
                        }
                        new_joined_object.push(temp_object);
                        left_position++;
                        if (left_position >= json_a.length) {
                            break;
                        }
                    }
                }
                break;
            }
        }
    }
    if (join_type == "left_outer") {

        left_item_added = false;

        while (true) {
            if (
                json_a[left_position][json_a_join_key] ==
                json_b[right_position][json_b_join_key]
            ) {
                temp_object = {};

                for (i = 0; i < json_a_view_keys.length; i++) {
                    temp_object[json_a_view_keys[i]] = json_a[left_position][json_a_view_keys[i]];
                }
                for (i = 0; i < json_b_view_keys.length; i++) {
                    temp_object[json_b_view_keys[i]] = json_b[right_position][json_b_view_keys[i]];
                }
                new_joined_object.push(temp_object);

                left_item_added = true;
                right_position++;

            } else if (
                json_a[left_position][json_a_join_key] <
                json_b[right_position][json_b_join_key]
            ) {
                if (left_item_added == false) {
                    temp_object = {};

                    for (i = 0; i < json_a_view_keys.length; i++) {
                        temp_object[json_a_view_keys[i]] = json_a[left_position][json_a_view_keys[i]];
                    }
                    for (i = 0; i < json_b_view_keys.length; i++) {
                        if (json_a_view_keys.indexOf(json_b_view_keys[i]) > -1) { continue }
                        temp_object[json_b_view_keys[i]] = null;
                    }
                    new_joined_object.push(temp_object);
                    left_item_added = true;
                }

                left_position++;
                left_item_added = false;

            } else if (
                json_a[left_position][json_a_join_key] >
                json_b[right_position][json_b_join_key]
            ) {

                right_position++;

            }
            // Condition to break the infinite loop
            if (left_position >= json_a.length || right_position >= json_b.length) {

                if (right_position >= json_b.length && left_position < json_a.length) {
                    temp_object = {};

                    for (i = 0; i < json_a_view_keys.length; i++) {
                        temp_object[json_a_view_keys[i]] = json_a[left_position][json_a_view_keys[i]];
                    }
                    for (i = 0; i < json_b_view_keys.length; i++) {
                        if (json_a_view_keys.indexOf(json_b_view_keys[i]) > -1) { continue }
                        temp_object[json_b_view_keys[i]] = null;
                    }
                    new_joined_object.push(temp_object);
                    left_position++;
                    if (left_position >= json_a.length) {
                        break;
                    }
                }
                break;
            }
        }
    }
    if (join_type == "right_outer") {

        right_item_added = false;

        while (true) {
            if (
                json_a[left_position][json_a_join_key] ==
                json_b[right_position][json_b_join_key]
            ) {
                temp_object = {};

                for (i = 0; i < json_a_view_keys.length; i++) {
                    temp_object[json_a_view_keys[i]] = json_a[left_position][json_a_view_keys[i]];
                }
                for (i = 0; i < json_b_view_keys.length; i++) {
                    temp_object[json_b_view_keys[i]] = json_b[right_position][json_b_view_keys[i]];
                }
                new_joined_object.push(temp_object);

                right_item_added = true;
                right_position++;
                right_item_added = false;

            } else if (
                json_a[left_position][json_a_join_key] <
                json_b[right_position][json_b_join_key]
            ) {

                left_position++;

            } else if (
                json_a[left_position][json_a_join_key] >
                json_b[right_position][json_b_join_key]
            ) {
                if (right_item_added == false) {
                    temp_object = {};

                    // TODO: I think this might be making the matching_key value be null and should be fixed
                    for (i = 0; i < json_a_view_keys.length; i++) {
                        temp_object[json_a_view_keys[i]] = null;
                    }
                    for (i = 0; i < json_b_view_keys.length; i++) {
                        temp_object[json_b_view_keys[i]] =
                            json_b[right_position][json_b_view_keys[i]];
                    }
                }
                new_joined_object.push(temp_object);
                right_item_added = true;
                right_position++;
                right_item_added = false;

            }
            // Condition to break the infinite loop
            if (left_position >= json_a.length || right_position >= json_b.length) {

                if (left_position >= json_a.length && right_position < json_b.length) {
                    while (true) {
                        temp_object = {};

                        // TODO: I think this might be making the matching_key value be null and should be fixed
                        for (i = 0; i < json_a_view_keys.length; i++) {
                            temp_object[json_a_view_keys[i]] = null;
                        }
                        for (i = 0; i < json_b_view_keys.length; i++) {
                            temp_object[json_b_view_keys[i]] =
                                json_b[right_position][json_b_view_keys[i]];
                        }
                        new_joined_object.push(temp_object);
                        right_position++;
                        if (right_position >= json_b.length) {
                            break;
                        }
                    }
                }
                break;
            }
        }
    }



    var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(new_joined_object, null, 2));
    var download_link = createNode("a")
    download_link.href = "data:" + data;
    if (join_type == "inner") {
        download_link.download = "inner_join.json";
    }
    if (join_type == "full_outer") {
        download_link.download = "full_outer_join.json";
    }
    if (join_type == "right_outer") {
        download_link.download = "right_outer_join.json";
    }
    if (join_type == "left_outer") {
        download_link.download = "left_outer_join.json";
    }
    
    download_link.innerHTML = "download JSON";
    download_link.id = "download_link_id"
    append(download_div, download_link)

}