# A simple JSON joiner

This is a simple JSON joiner that was written primarily to be user accessible, and as a learning experience.


## Requirements

This app can be run easily.

Just clone the repo and open joiner.html in a browser (Chrome preferably).

You can also run it with Docker where it's backed by a web server:

```docker run -p 8080:8080 rlake80s/json_joiner```

Then visit http://localhost:8080/joiner.html in a browser. 

